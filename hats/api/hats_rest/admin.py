from django.contrib import admin

# Register your models here.
from .models import Hats


@admin.register(Hats)
class HatsAdmin(admin.ModelAdmin):
    pass