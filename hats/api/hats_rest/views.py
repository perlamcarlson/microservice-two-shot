from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Hats, LocationVO
import json

# Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "section_number", "shelf_number", "import_href"]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "name",
        "color",
        "style",
        "fabric",
        "picture",
    ]
    encoder = {
        "Location" : LocationVOEncoder()
    }


@require_http_methods(["GET", "POST"])
def show_hats(request):

    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
    )
    else:
        content = json.loads(request.body)
        print(content)
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False,
        )


