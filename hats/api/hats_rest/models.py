from django.db import models

# Create your models here.

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)
    import_href = models.CharField(max_length=200, unique=True)



class Hats(models.Model):
    name = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    style = models.CharField(max_length=100)
    fabric = models.CharField(max_length=100)
    picture = models.URLField(max_length=200, null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE,
        null=True,
    )
    def __str__(self):
        return self.name

    
