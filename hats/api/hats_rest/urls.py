from django.contrib import admin
from django.urls import path
from .views import show_hats

urlpatterns = [
    #path('admin/', admin.site.urls),
    path("hats/", show_hats, name="show_hats")
]

