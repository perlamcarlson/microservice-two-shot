


function HatsList(props) {
    if (props.hats === undefined) {
        return null;
      }

    return (
        <table className="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Color</th>
                    <th>Style</th>
                    <th>Fabric</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    return (
                        <tr>
                        <td>{ hat.name }</td>
                        <td>{ hat.color }</td>
                        <td>{ hat.style }</td>
                        <td>{ hat.fabric }</td>
                        <td>{ hat.id }</td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default HatsList;
