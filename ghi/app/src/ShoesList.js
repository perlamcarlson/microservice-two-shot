function ShoesList(props) {
    console.log("Shoes List");
    console.log(props);
    if (props.shoes === undefined){
        return null;
    }
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>ShoesList</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map(shoe => {
            return (
              <tr key={ shoe.id }>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.bin }</td>
            </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
  
  export default ShoesList;