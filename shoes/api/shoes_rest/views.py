from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import ShoeBinVo, Shoes
from common.json import ModelEncoder


class ShoeBinVOEncoder(ModelEncoder):
    model = ShoeBinVo
    properties = ["closet_name", "bin_number", "bin_size"]
    

class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [ 
        "manufacturer", 
        "model_name", 
        "color",
        "picture_url", 
        "bin_number",
        "id"
        ]
    encoders = {
        "shoeBin": ShoeBinVOEncoder(),
        }


@require_http_methods(["GET", "POST"])
def api_shoes(request):
    """
    Collection RESTful API handler for Location objects in
    the wardrobe.

    GET:
    Returns a dictionary with a single key "locations" which
    is a list of the closet name, section number, and shelf
    number for the location, along with its href and id.

    {
        "locations": [
            {
                "id": database id for the location,
                "closet_name": location's closet name,
                "section_number": the number of the wardrobe section,
                "shelf_number": the number of the shelf,
                "href": URL to the location,
            },
            ...
        ]
    }

    POST:
    Creates a location resource and returns its details.
    {
        "closet_name": location's closet name,
        "section_number": the number of the wardrobe section,
        "shelf_number": the number of the shelf,
    }
    """
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    else:
        content = json.loads(request.body)
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesListEncoder,
            safe=False,
        )
# Create your views here.
