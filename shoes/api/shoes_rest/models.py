from django.db import models



class ShoeBinVo(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

class Shoes(models.Model):
    manufacturer = models.CharField(max_length=150)
    model_name = models.CharField(max_length=150)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(max_length=200, null=True)
    bin_number = models.PositiveSmallIntegerField()
    bin = models.ForeignKey(ShoeBinVo, related_name="bin",
    on_delete=models.CASCADE, null=True,
    )
    def __str__(self):
        return self.name 




    
    



# Create your models here.
